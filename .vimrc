execute pathogen#infect()
syntax on
filetype plugin indent on


set t_Co=256
set term=screen-256color
set smartindent
colorscheme wombat256mod

set relativenumber
set wildmode=longest,list,full
set wildmenu
set cursorline

let NERDTreeShowBookmarks=1

map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" VIM as Culculator
ino <C-l> <C-O>yiW<End>=<C-R>=<C-R>0<CR>

" Shift is too far
nnoremap ; :

" Foldng
"set foldmethod=syntax
"nnoremap <Space> za
"nnoremap F zR
"nnoremap F zM

" Copy to clipboard
vmap <C-c> "+y"

" -- Tabs --
set expandtab "Tabs are spaces
set tabstop=4 "Visual 4 spaces
set softtabstop=4 "Insert 4 spaces
set shiftwidth=4 "size of an indent
set shiftround

let mapleader="," "Leader is comma.

nnoremap <leader>. :NERDTreeToggle<CR>

" Moving lines
" Normal mode
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
"
" Insert mode
inoremap <C-j> <ESC>:m .+1<CR>==gi
inoremap <C-k> <ESC>:m .-2<CR>==gi
"
" Visual mode
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" I like tabs more than buffers
nnoremap tn :tabnew<CR>
nnoremap tc :tabclose<CR>
nnoremap t> :tabnext<CR>
nnoremap t< :tabprev<CR>
"F2 to toggle paste
set pastetoggle=<F2>

" Autoreload vimrc on save
autocmd! bufwritepost .vimrc source %

set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//

" Autoclose bracket
imap {<cr> {<cr>}<c-o>O


" Adding SnipMate HTML support to PHP files
let g:snipMate = {}
let g:snipMate.scope_aliases = {}
let g:snipMate.scope_aliases['php'] = 'php,javascript,html'

" Very END
function! PhpSyntaxOverride()
  hi! def link phpDocTags  phpDefine
  hi! def link phpDocParam phpType
endfunction

augroup phpSyntaxOverride
  autocmd!
  autocmd FileType php call PhpSyntaxOverride()
augroup END


